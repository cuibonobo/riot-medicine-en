\chapter{Other Gear}

\epigraph{
Duct tape is not a perfect solution to anything.
But with a little creativity, in a pinch, it's an adequate solution to just about everything.
}{Jamie Hyneman, \textit{Mythbusters}\supercite{mythbusters-duct-tape}}

\noindent
This chapter will cover additional gear you will want to consider wearing or carrying to an action.
Some of these things are comfort items that make your job easier and less arduous, and others will help you with your duties.

\section*{Essentials}

Describing the following items as ``essentials'' may be an overstatement, but they are exceptionally useful to bring to every action.

\subsection*{Food and Water}

You should always bring food and water for yourself.
A reusable water bottle, aside from being good for the environment, is less likely to burst if you are hit.
To encourage hydration, you may want to bring flavored powders with electrolytes to add to you water.
For food, energy bars and oatmeal bars are good choices because they are small but high in calories and will not spoil.
Some medics take a more minimal approach and bring just dense bread.
Regardless, you may need to experiment with what food you can tolerate while exercising or nervous.

\subsection*{Duct Tape}

Duct tape is a generally very useful item.
It can be used to repair your bag or clothing as well as seal a small crack in a full face respirator.
It can be used to hold a bandage in place or as a bandage itself.\footnotemark[1]
Duct tape is so useful, it has been carried on all NASA mission since the early 1960's and even was used to repair the Apollo 13 carbon dioxide filters.\supercite{apollo-13-duct-tape}
Your duct tape should be black to avoid marking patients who are members of the Black Bloc\index{Black Bloc}.

\footnotetext[1]{
Duct tape has latex in it and may cause an allergic response in patients.
}

\subsection*{Phone}

Your phone is your primary communication and intelligence gathering device.
Encrypted messaging apps will allow you to coordinate with other medics and organizers.
You may have scouts who report information directly to you.
Twitter or other social media\index{social media} feeds may provide useful live updates on blockades, protester movements, opposition movements, and police movements.

Whether or not you should bring a phone or what kind of phone you should bring along with the security implications associated with phones is discussed in \fullref{ch:opsec}.

\subsection*{Wallet}

Depending on local laws\index{legality}, you may be required to present identification to law enforcement, even in non-protest environments.
Identification may help you with getting bailed out of jail.
Carrying a health insurance card may help you receive prompt medical care if you are injured and taken to a hospital.
Cash, bank cards, or credit cards are generally useful.
If you are concerned about the security implications of carrying IDs on you person at actions, a discussion of how IDs can be targeted by digital surveillance is discussed in \fullref{ch:opsec}.

\subsection*{Permanent Marker}

A black, permanent marker with a large tip is useful for writing emergency contact information on your body.
Phones may become damaged, and normal ink may be rubbed or washed away.
When writing a phone number on your body, ensure it is not visible as it may cause you or your emergency contact to get doxxed or harassed\index{doxxing}.
Permanent markers can also be used to write notes on yourself or the patient.

% TODO footnote about Florida(?) activists getting their contact doxxed because it was on their arm and made the local news

\subsection*{Notepad}

A notepad is useful for taking down measurements and notes from a patient.
This may help with determining if their condition is degrading or improving, and these notes may be useful for other medical professionals who take over care.
Notepads can also be used to document police badge numbers or the names of arrestees.
This information can be passed on to anti-repression\index{anti-repression} and jail support\index{jail support} group.

\subsection*{Paper Map}

You should carry paper map with routes, rally points, and other points of interest.
If you expect precipitation, you may want to consider using a plastic sleeve or laminating your map.
A hard copy of the map for your action is a good back up even if you have a phone.
Cell service may be cut, you phone may run out of charge or become damaged, or you may not want to constantly be fishing for your phone while jogging with a crowd.

\subsection*{Tissues}

Packages of tissues are useful because they are a more pleasant alternative to gauze to give to patients who need to blow their nose or dry their eyes.
Tissues also carry the psychological benefit of feeling more familiar than gauze to help patients regain a sense of normalcy after a traumatizing experience.

\subsection*{Watch}

A wristwatch or pocket watch is useful for keeping track of time without having to pull out your phone.
A watch is also useful for measuring heart rate.
Your watch should be digital, or if it is analog, its should tick (not rotate smoothly) to get more accurate readings.
You watch should be water resistant, and the straps should be plastic.
Textile strap, leather straps, and metal links will absorb and trap more riot control agent and bodily fluids.

\subsection*{Personal Medication}

If you take medications either regularly or in emergency situations, you should consider carrying several days worth of medication with you in the event you are arrested\index{arrest!medics@of medics}.
You may also want to take a photocopy of your prescription to help reduce the likelihood that your medication is confiscated during your arrest.
These medications should be stored separated from the medication you hand out, preferably in a pocket or small bag that is unlikely to be confiscated or lost during your arrest.

\subsection*{Batteries}

For every electronic device you carry that takes replaceable batteries, you should bring spares.
This means flashlights, blood glucose monitors, or pulse oximeters.

\section*{Nice-to-Haves}

The following items are generally useful and are things you should consider bringing with you.

\subsection*{Small Bag}

A second, small bag is useful for carrying frequently used items like examination gloves, gauze, medical tape, and trauma shears.
A waist bag (fanny pack, hip bag), utility belt, or utility pouch (either strapped to your thigh or slung over your shoulder) provides easy access to these items.
You may find this convenient for quickly treating patients while walking instead of stopping and removing your main bag.

\subsection*{Baseball Hat}

A baseball hat can protect your eyes and face from direct sunlight, but a reason you may want to wear one to actions is the brim can partially shield your eyes from sprayed chemical agents.
You may not always be wearing your eye protection, or an incident may occur without warning.
By quickly tilting your head down, you can avoid most of the chemical agent in a direct spray.
Likewise, this can be done to help protect your identity if you spot photographers.
A hat with medic markings also makes you more visible in a crowd.

\subsection*{Umbrella}

An umbrella can be used to keep you dry in the rain or provide shade in the heat.
They can be used to block cameras from the police, fascists, or nosy journalists\index{journalists!patient privacy@in patient privacy}\footnotemark[2].
They also provide some measure of protection against water cannons and sprayed riot control agents.

\footnotetext[2]{
I'm really not trying to be anti-journalist in this book, but most journalists are not on ``our side.''
Many are actively against us.
Regardless, even well intentioned journalists can cause harm by revealing our identities.
}

\subsection*{Backpack Rain Cover}

A backpack rain cover is a waterproof cover that can be pulled over a backpack to protect it from the rain.
These are useful during rainy actions or actions where water cannons may be deployed.
Riot control agents may be mixed with the water from water cannons, and contaminated equipment may be harmful to patients.

\subsection*{Phone Battery Pack}

If you bring a phone, a spare battery pack and charging cable will ensure your phone has charge for the entire action.
This is worth carrying because you may become kettled or may stay late to support an occupation well beyond what you anticipated.

\subsection*{Two-Way Radio}
\index{two-way radios|(}

A two-way radio is a radio that can both transmit and receive a signal.
Medic teams may choose to use these in conjunction with or instead of phones.
Radios have the advantage of not requiring external infrastructure to operate.
Large crowds can overload cell sites and prevent phone calls, SMS, or encrypted communications using mobile data from being sent or received.
Radios have the disadvantage or being an additional cost and weight as well as requiring one for every buddy pair.
The lack of encryption makes radio communication susceptible to interception, both for collection and real-time eavesdropping.
Medic teams have reported poor range and signal clarity with basic two-way radios in the field in urban environments because of large buildings and in rural environments because of hills or dense forest.
Radios should be field tested before they can be relied on.

Two-way radios are discussed in more detail in \fullref{ch:radio_operation}.

\index{two-way radios|)}

\subsection*{Sunblock}

Sunblock, also called sunscreen or suntan lotion, is a topical product that blocks a percentage of ultraviolet rays.
Your sunblock should be water-based.
Oil-based sunblock will cause \gls{riot control agent}\index{riot control agents!considerations@considerations for} to bind to your skin.
Your sunblock should be water-proof to prevent it from being washed off by your sweat, water cannons, or runoff from decontaminating patients.
Your sunblock should be at least \acrshort{SPF} 30.

Use of sunblock prevents skin damage and reduces the risk of cancer.
However, the primary reason to use sunblock as a medic is that riot control agents\index{riot control agents!considerations@considerations for} will be more painful on sunburned skin.

\subsection*{Flashlight}

Flashlights are useful for visibility while working or simply while walking as well as signalling in the dark.
If your helmet has a built-in flashlight mount, you should acquire a light that mounts to it.
Usually these lights have a shape that makes them useable as a handheld light when you are not wearing your helmet.
These are also recommended as they do not require your hands to use.
Chest-mounted lights that placed in a pocket or clipped to your bag are another hands free option.
If nothing else, a small hand-held flashlight can be carried.
Full-sized flashlights (the kind cops stereotypically carry) are excessively large and heavy, and they are liable to be confiscated by the police as a blunt weapon.

\subsection*{Glasses Case}

If you wear corrective glasses, you should bring a hard glasses case to store your glasses.
This is particularly useful while wearing a respirator and goggles, but you also may want to remove your glasses before you expect conflict to prevent damage to them.

\subsection*{Contact Lenses}
\index{contact lenses|(}

As previously noted, corrective lenses are preferred to contact lenses due to increased incapacitation if riot control agents are deployed.
You may want to consider bringing a pair of contacts lenses as a spare in the event your corrective lenses are damaged or if you have to remove your first pair due to contamination\index{riot control agents!considerations@considerations for}.
You may also need replacement lenses if you are arrested\index{arrest!medics@of medics} and need to remove your lenses while jailed.

\index{contact lenses|)}

\section*{Clothing Choices}
\index{clothing|(}

Choice of clothing depends a bit on the political climate where you are operating as well as your role.
Discussion of tactical choice of attire is discussed in \fullref{ch:general_tactics}.
Weather should be considered as well.

In all cases, layering is recommended as if allows you to more precisely control how much you are insulated from cold, wind, and rain.
\Glspl{riot control agent}\index{riot control agents!considerations@considerations for} bind to cotton, wool, and other natural textiles, but synthetic material such as nylon can melt to your skin as a result of coming in contact with explosives, other pyrotechnics, or burning tear gas cannisters.
Layering allows you to wear both a heat resistant layer and a riot control agent resistant layer.

You should consider wearing thick cargo pants or shorts with many pockets, in particular work pants with velcro pockets.
Likewise, specialty work vests for EMS personnel, hunting vests, and work vests offer many external pockets.
Frequently used items should be placed in pockets with fasteners for rapid access as you will not want to constantly have to stop to get things out of your bag.

Other clothing items that tend to be useful are a neck gaiter or keffiyeh (also known as a shemagh or pali scarf).
These are useful for warmth, to conceal your identity, and to protect your neck and face against sprayed riot control agents.
Additionally, a keffiyeh can be used as a triangle bandage.

Some medics bring a change of clothing in an air-tight plastic bag to change in to in case they are contaminated with \gls{riot control agent}\index{riot control agents!considerations@considerations for}.
If you are operating in an environment where medics are classified as protesters and not as neutral caregivers, you may want to consider plain clothes for getting to and from an action\index{anti-repression} to prevent you from detained before the action or arrested\index{arrest!medics@of medics} after.
This takes up significant space and may be heavy.
This volume and weight may be better served with additional medical gear.

\index{clothing|)}
