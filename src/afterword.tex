\chapter{Afterword}

\epigraph{Another world is possible.}{Anarchist aphorism}

\noindent
By reading this book, you have hopefully learned enough about riot medicine and to be able to provide care for those injured as they strive toward a world free of hierarchy and domination.

Being a medic is no simple task, but neither is being a revolutionary.
Maybe you don't think of yourself as the latter, or maybe you do.
Regardless, the very act deciding to care and provide for one another is a radical act.
Supporting social movements and providing care to others is insurrectionary.

Revolution is not a singular point that neatly divides time into some ``before'' and mythic ``after.''
We envision another world, and through a perpetual process of bettering ourselves and the society in which we exist, we hope to bring bits of this imagined other world into existence.
We, the global community of anarchists, the broader coalition of the left, move forever towards a better society without ever reaching some final resting place where we are ``done.''

This process is tiring.
It's exhausting.
And your work will never be done.
There will always be someone to heal.

Mutual aid is such a beautiful concept, and using this simple idea, communities can accomplish so much.
What we can do is stand side by side in solidarity for our causes: a world without borders or nations, gods or masters.
To these ends, I often think of this quote from José Buenaventura Durruti Dumange from 1936:\supercite[pp. 478]{durruti-spanish-rev}

\begin{displayquote}
We have always lived in slums and holes in the wall.
We will know how to accommodate ourselves for a time.
For, you must not forget, we can also build.
It is we the workers who built these palaces and cities here in Spain and in America and everywhere.
We, the workers, can build others to take their place.
And better ones!
We are not in the least afraid of ruins.
We are going to inherit the earth; there is not the slightest doubt about that.
The bourgeoisie might blast and ruin its own world before it leaves the stage of history.
We carry a new world here, in our hearts.
That world is growing this minute.
\end{displayquote}

But for all of this longing for another world, no optimism can overcome the reality that we very likely will not see an end to capitalism, imperialism, or fascism in our lifetimes.
You cannot win every battle, and you cannot save everyone from pain or death.
What you can do is minimize it.
Don't focus on who you couldn't heal or save.
Focus on the people you helped yesterday and those you can help tomorrow.
Your care can improve their lives, even if it's a simple as cleaning pepper spray from their eyes.
You may forget their face and they yours, but they will remember that on some shitty, cold day, an unnamed comrade was there to help them.

And that is what we are here to do.
To help when we can, however we can, to heal and care for one another, and to show that we are all in this together.
