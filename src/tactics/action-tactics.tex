\chapter{Action Tactics}

\epigraph{Crises precipitate change.}{Deltron 3030, \textit{Virus}}

\index{tactics!action|(}

\noindent
A key difference between clinical medicine and riot medicine is that in a clinical setting, patients arrive to the doctor in a stationary, well equipped, and controlled environment.
This contrasts to medics who need to seek out patients and may be intentionally or incidentally blocked from being able to provide care.
In this sense, the practice of riot medicine shares many characteristics with both lifeguarding and combat medicine.

In this chapter, actions are loosely split into two categories with no defined boundaries.
This is done to help talk about the tactics that make sense at actions that are generally more peaceful and generally more confrontational.
For our purposes, these are called demonstrations and riots respectively.
This isn't claiming that organizers or attendees always start an action with the explicit intent of having a more reserved action or a more rowdy one (some may have goals and other may let things develop organically), and this isn't claiming that actions always neatly fall into one category or the other.
Actions may begin as one, transition to the other, and then back again as the day goes on, and as such, your choice of tactics may need change too.

\section*{Coordination}

In \fullref{ch:organizational_structures}, there was some discussion about organizing within the local community and with other medics and collectives as a means of effectively providing medical care.
Long-term planning and continuously attending actions helps build familiarity and repeatable habits so others know what to expect from medics, but more specifically there are certain way medics should coordinate with others at every action.

First, medics should check-in with the organizers or major groups at an action.
When medics make a habit of checking in, organizers will know that medics are or are not present instead of medics' presence seeming random.
When organizers and other groups know that medics are in attendance, they will know to seek them out should someone require assistance instead of waiting for traditional EMS to arrive.

When you check-in, you should ask if there are other medics present so you can speak with them.
This will allow you to coordinate your positioning with them to provide better coverage of the event as well as having an idea of how they are equipped and what their competencies are.
You are encouraged to work with other collectives as this will facilitate knowledge sharing as well as solidarity.
Members of different collectives are encouraged to work closely with each other at actions.

As part of coordinating with organizers, groups, and other medic collectives, establishing a means of communicating is important.
This may mean exchanging phone numbers or setting up a temporary group chat using an encrypted messaging app.
Rapid coordination over large distances can be extremely helpful, and organizers are often informed of injuries which they can then relay to you.

\section*{Forming Teams}
\index{medics!teams|(}

During actions, there may be more medics than you and your buddy, and it may be advisable to split into teams.
Having ten medics all at the same location generally does not make sense.
Spreading medics across the length of a march or across an action increases the chance that medics will be able to promptly reach patients.
This is of particular importance if the area is large, there are multiple places where violence may break out, or there a chance that police or opposition movements or tactics could partition the action into multiple disjoint groups.

Because medics should not operate alone, teams should be at least two medics.
In general, only three medics can treat a patient at the same time, so with allowance for one spotter, teams should have a maximum of four or five medics.
Generally, teams of two or three seem to make the most sense since other protesters can be recruited as spotters or to provide protection, so allocating one slot for someone dedicated to this job tends to be unnecessary.
Once a team grows to six, it typically makes sense to split it into two teams of three to provide better coverage.

Friendships, interpersonal relationships, and minor disagreements over politics can give rise to natural formations of teams, but assuming all medics are able to get along, the dividing of medics into teams should generally be done around experience in riots, medical qualification, athleticism, and tolerance for risk.
Some guideline are as follows:

\begin{itemize}
    \item Place medics with more experience at riots on teams with medics with less experience at riots to they can ensure good positioning as well as team safety.
    \item Place medics with greater medical qualifications on teams with medics with lesser qualifications so the highest number of teams can perform certain procedures (e.g., the use of adjuncts for advanced airway management).
    \item Place medics who are more athletic on the same team so that if part of an action begins running, at least one team can match the pace.
    \item Place medics with greater athleticism on to separate teams so they can be the ``pack mules'' who carry the majority of the equipment to prevent tiring out less athletic medics.
    \item Place medics with similar tolerances for risk on teams together so that no one is pressured into more dangerous situations than they are comfortable with.
    \item Place medics with similar \acrshort{PPE} on teams together so that no member of a team faces increased risk of injury during any activity (e.g., all team members have helmets).
    \item Place medics who have to leave early on teams together to prevent their departure from breaking up teams or leaving anyone alone or unequipped.
    \item Increase teams sizes when actions are expected to be more confrontational or violent to provide safety in numbers.
    \item Decrease team sizes when actions are expected to be peaceful or stationary to provide better coverage.
\end{itemize}

It is not possible to follow all of these guidelines as some are directly contradictory.
When considering which guideline (if any) to use for splitting teams, it is useful to discuss with your collective how they think the day's action will play out.
Example scenarios are provided below.
They are hypothetical and meant to illustrate how one might assign teams, and when reading them, you should not assume all actions will play out like what is described here.

\subsubsection*{Example 1}

A local civil rights group has arranged a march through downtown, starting at a park and ending at city hall.
The march is expected to have only light police presence based off similar actions in the past and the posts made by the local police department's Twitter account.
This is confirmed by the medics when they arrive.
An estimated 10,000 participants are attending.
There are six medics present: 1 nurse, 1 \acrshort{EMT} (basic), 2 more experienced medics, and 2 less experienced medics.

As sensible arrangement would be to make three teams.
The nurse and EMT each pair up with the one of less experienced medics, and the two more experienced medics pair with each other.
Should it be needed, the nurse and EMT can fully render care in most situations, and the less experienced medics can learn and receive guidance.
The more experienced medics may have less medical knowledge, but because they are experienced, they are unlikely to panic should they encounter injuries or illnesses that are more severe than they know how to handle.

\subsubsection*{Example 2}

Fascists from around your region and beyond have converged on your town following some perceived injustice.
Because of the rapid mobilization by the far-right, police presence is minimal.
Antifascists and liberals are counter-protesting the spontaneous demonstration, but are outnumbered.
The attendees are 3,000 fascists, 200 police, 400 antifascists, 800 liberals, and 5 medics of varying skill level.

The medics analyze the situation and coordinate with the radicals to see what they plan on doing.
Because there are so few police, the risk of the action partitioning because of the police is low as is the risk of kettling.
The medics determined that the main risk is not violence between police and the antifascists, but between the fascists and the counter-demonstrators.
Most antifascists do not plan on directly engaging with the fascists because they will be protecting the liberals who were not expecting a fight.

The medics decide to stick to one team of 5 because the action is small enough and there is too great a risk of violence.
They will provide roaming coverage around the liberal participants since they are less accustomed to violence, and any hot engagements between antifascists and fascists will likely be too dangerous for medics to be immediately useful.

\subsection*{Clustering}

A natural tendency is for people to cluster with friends and comrades, and medics are no exception.
When you see other medics you know at actions, you may be tempted to hang out with them or walk with them.
It is recommended that you avoid prolonged time with other medics as this decreases coverage and changes the focus to socializing instead of scanning the crowd and making continuous tactical assessments.
The larger risk is that actions may become partitioned by the police.
If the medics are clustered, they may be cut off from parts of the action, and people may be left without medical support.

Medics are human too, and there is the need to relax a bit even while at demos.
Being ``on'' all the time can be draining, and the previous recommendation against clustering is said strictly from a tactical perspective.
When it comes to providing care and being aware of your surroundings, socializing in large groups can be counterproductive.

\index{medics!teams|)}

\section*{Scouting}
\index{scouts|(}

Related to coordination with organizers and other medics collectives, having strong connections to local comrades allows you to recruit scouts and spotters at actions.
Scouts can be useful for medics to notify medics of demonstrators' tactics that might trigger a response from fascists or the State.
Scouts may be members of the Black Bloc\index{Black Bloc}, journalists\index{journalists}, or more casual protesters.
Scouts are useful as they do not require any medical training, and they can operate with less of the restrictions of neutrality medics might self-impose.
If you work with scouts, it is generally useful to have them relay to you the following information:

\begin{itemize}
    \item The location, movements, and strength of police, fascists, and friendlies
    \item The presence of blockades and whether they were placed by the police, friendly demonstrators, or opposing demonstrators
    \item The position and size of kettles
    \item Locations where there are incipient or current confrontations
    \item Any injuries including location and severity
\end{itemize}

You may not be able to always react to all this information, but it can help you prioritize what situations to attend to as well as give an overview of what is happening during an action.
Use of scouts may be particularly useful if the police inhibit medics' movements during actions, especially if the action is large.

\index{scouts|)}

\section*{Positioning}

Knowing where to be can be challenge to providing medical care.
If you cannot find patients, or if you cannot reach them, you will be unable to help them.

\subsection*{At Demonstrations}

During demonstrations, most injuries and illness a medic treats will not be trauma from confrontation with the police of other opponents, but from the environment or the individual's inability to adequately take care of themself over the course of the day.
You will typically see heat exhaustion, hypoglycemia (diabetic or otherwise), dehydration, nausea, and general feelings of unwellness.
With this in mind, the position of medics is best done more akin to how lifeguards position themselves while watching a pool.
Medics should attempt to maximize coverage over the main body of the demonstration.

Maximizing coverage may mean teams that are only two medics instead of the slightly larger teams of three or four that might make more sense at actions that are more confrontational.
These teams should stagger themselves along alternating sides of the march route or around the body of a stationary demonstration.
During a march that does not appear to be confrontational, medics do not need to be directly at the head since there is unlikely to be conflict and they are better placed a bit further back where they can see more of the body of the march.
It is also useful to have one team of ``sweepers'' at the end to catch stragglers who are exhausted or may have fallen during the march.
If an action is stationary, medics should periodically walk through the crowd to scan for unwell participants.
Medics also should be mindful to not only look ahead but to frequently check behind them and away from the main body as injured or ill persons may move far to the sides of the main route in search of shade or a place to sit.

Unless it is unsafe or unpermitted, medics should generally position themselves on the sides of crowds instead of in the main body.
This is done to give increased visibility into the crowd.

During marches, medics may have a tendency to walk too fast and bunch up at the front of a march because the sides are relatively clear.
It is a good practice to pick a fixed group or banner in the crowd and use it to keep a steady pace.

\subsection*{At Riots}

For actions that are expected to be more confrontational, or as a previously calm action moves towards confrontation, how medics should position themselves becomes more complex.
It requires greater understanding of the area, police tactics, friendly tactics, and opposition tactics.
The basic question your collective should ask is: what do we think is going to happen and where?

\triplesubsection{Route}
Is this a march or an event that has a known (public or private) route?
What is that route? How long will it take to traverse the route?
Do radicals plan on deviating from the public route?
If the route deviates from the known route, the police will likely intervene.
Has this route been used before, and are there points where conflicts have happened before?
Are there choke points along the route where protesters will be forced into close proximity with with the police?
These may lead to confrontations.

\triplesubsection{Points of interest}
Are there major points of interest that may lead to conflict?
For example, police stations, monuments (including both those supported and opposed by the action), memorials, or buildings that may be the target of property destruction.
Will the action take place in a neighborhood with racial, ethnic, or national divisions?

\triplesubsection{With non-state opposition}
Is your action the counter-protest? If so, what is the route for the main protest? Answer the previous questions but from the opposition's point of view.
What are the start and end points of their route?
The start and end of both your action and theirs tend to be locations of conflict.
Will your protest attack the other protest or vice versa?
Will anyone form blockades?
Where will these be? What is the typical police response to blockades? Being present at a blockade made by comrades is useful as there tend to be injuries when they are cleared out.

\quadruplesubsection{Arrival and departure}
Both your arrival and departure points for an action, as well as those of your opposition, are places of potential conflict.
Opposing sides may attack each other at these locations, and those protesting against fascists at these locations may be attacked by the police.
You may need to arrive early and stay until the end to ensure all protesters, even those fighting to the bitter end, can receive care.

\triplesubsection{Mood}
What is the mood of the action?
Is is calm or are there radicals looking to fight the State?
Is this a recurring action such as May Day that is known for being conflict heavy?
Actions may start violent and then lose energy throughout the day, or they may start comparatively calm and then gain momentum as they go on, climaxing into a street brawl with cops.
Having a sense for the mood of the will help you decided how close to radicals you need to be.

\triplesubsection{Radicals}
Is there a group of radicals or a Black Bloc\index{Black Bloc}?
The Bloc is a both a source and sink of violence at actions.
The police will target it because they can ``get away'' with beating rowdy anarchists in the public eye, and likewise the Bloc is often interested in fighting police and fascists.
Being adjacent to the Bloc makes it easy to quickly render care, but if you are not sufficiently distinct from the Bloc, you may become a target yourself.

\triplesubsection{State tactics}
What sort of police tactics do you expect for the day?
What sort of forces will be out?
Uniformed cops, riot cops, water cannons, horse mounted police, bike police, motorcycle mounted police?
Have cops like this been deployed before, and what happened when they were?
Is kettling a tactic?
Do the police attack bystanders or is it limited to protesters or even just the Bloc?
Are medics targeted by the police, or are medics relatively safe?
The more medics are targeted, the more they should try to stay with the main body of a protest.

\index{helicopters!riots@in riots|(}
\triplesubsection{Helicopters}
Are helicopters deployed?
During an action, if you are unsure where to position yourself, you may want to consider making your way directly below them.
Helicopters are often above points of conflict or potential conflict.
\index{helicopters!riots@in riots|)}

\triplesubsection{Stay late}
In addition to staying late to provide care at departure points, police may look to pick off lone groups of protesters or antifascists after an action is ``officially'' over.
You may need to stay late to help these individuals, though this may make you a target yourself.
Note that if you are uniformed, police may treat your continued presence as an escalation in so much as you encourage protesters to stay longer and get rowdier while support is still there.

\subsubsection*{At the Action}

Once you have a general idea of how the day might play out, positioning is often done by answering these simple questions: where will there be violence, and how close can I safely get to it?

Some general rules that are helpful for positioning are as follows:

\begin{itemize}
    \item Be present at large blockades
    \item Be near the counter-protests at the start and end of fascist marches as there may be violence as counter-protesters try to disrupt the march before it begins or fight them after
    \item Be near the front of a march or near the Bloc (often the same place)
\end{itemize}

You will need to be close enough to the activities to not be cut off or kettled, preventing you from reaching patients.
However, kettles themselves may be something you want to be in if you have reason to believe there are injuries in the kettle, something that is often true if the police are acting violently toward protesters.
You may not be able to enter a kettle, and once in, you may not be able to leave.
Letting yourself get kettled is generally only advisable if you know there are other teams who can continue roaming.

\clearpage % NOTE spacing needed here, maybe not in the future
\begin{war-story}{Voluntarily Kettled}{Anonymous}
Following days of rioting by neo-nazis, we were out in service again for a left/liberal counter demonstration.
On this night, there were multiple teams present, some we knew well, and some we were only loosely coordinated with.

Almost immediately, the radical elements of the left moved towards where the neo-nazis had gathered and ended up in fights with cops who were protecting them.
Cops in riot gear, horse mounted cops, and vans quickly pulled up and drove the Bloc back into a commercial plaza where some 150 people got kettled.

My team had the opportunity to stay out after the kettle had already closed, and we discussed whether we should go in too.
We knew there were other teams out in the city, and we weren't sure if there would be any patients for us to help or even ones that we would be able to find.
We'd already seen cops beating protesters and rocks flying, so we guessed that there were injured comrades in the kettle.
With the camouflage of respectability from our professional looking uniforms, we were able to talk our way in and get to work.

Two patients required ambulances, but the police wouldn't let the EMS personnel through to evacuate them.
Luckily, one of our comrades was a certified paramedic and was citing legal code at the cops and writing down their badge numbers until they conceded that we could do the evacuations.
EMS was able to do their job, and without us there, the patients probably would not have gotten the medical attention they needed.

We were stuck in the kettle until late into the night, but even though we missed the exciting ``action,'' it was the right decision to join the other protesters in the kettle.
\end{war-story}

\section*{Movement}

There are some guidelines about movement to keep in mind while acting as a medic.

\subsection*{Walk, Don't Run}

When moving at an action, a general rule is to avoid running or jogging when no one else is.
Anyone running at a demo can incite panic, especially if they are highly visible and seemingly official like medics may be.
Is the person running because of police violence? A vehicle attack? A knife attack?
What ever the case is, running can make people uneasy, especially if you are in a uniform because the implication is that you have a very good reason to be running.

In most cases, if you you must move fast, walk quickly.
This gives you time to collect information about what is happening.
Running will usually only save a couple of seconds over most distances, and as you learn how to position yourself, you will find it becomes less and less necessary to have to move more than a few tens of meters to reach a patient.
Further, not running makes it easier for a team to sick together.
One team member taking off in a sprint to reach a patient leaves the rest of the team behind.
When you move more slowly, you move together.

There are some clear exceptions to this, such as the presence of danger that you must avoid or if the action you are supporting is running and you must keep up.
Teams may need to move through choke-points or police lines before a route closes.
Often this is a quick scurry as police start to create a kettle or block off a street, but sometimes this might mean sustained jogging to get a head of a large police presence that is sequentially blocking off all access to a street, park, or other waypoint.

\subsection*{In a Crowd}

When moving through a crowd, it is very easy to become separated from your team.
This can be through simple inattention or accidentally being cut off by the movements of the crowd itself.
The priority should generally be to keep the team together, not to reach the destination quickly.

Teams should move in a line with one person acting as a trailblazer to find a path through the crowd and everyone else following in the path they've created.
The trailblazer has the responsibility of ensuring they are moving at a manageable pace, avoiding abrupt path changes (to a reasonable degree), and avoiding moving through tight spaces that might separate the team.
Those following have the responsibility of not losing the person in front of them.
Everyone, including the trailblazer has the responsibility of not losing the person behind them meaning they should frequently check that no one behind them has become separated.

To keep the team together, firmly grab a strap or handle of the bag of the person before you.
Feel for the tension or weight on your bag from the person behind you.
If you lose your grip, shout for the person in front of you to slow down.
If you feel someone lose their grip or hear them shout, pull on the person in front of you and relay that the team needs to slow down.
For an example of this, see \autoref{fig:moving_as_a_team}.

\begin{figure}[htbp]
\centering
\caption{Moving as a Team\supercite{zerocoil}}
\label{fig:moving_as_a_team}
\includesvg[height=6cm, keepaspectratio]{moving-as-a-team}
\end{figure}

\section*{Search and Rescue}
\index{search and rescue|(}

Ill or injured protesters may not be aware that medics are present or they may be unable to find them.
To get aid, they may call EMS themselves.
When EMS arrives, they may not be able to locate the patient in the crowd, and medics may be recruited to help find the patient.

This is a time when it is appropriate for medics to split up from their buddies in an attempt to cover as much ground as possible.
Coordinate with EMS on how to search.
Ensure that whatever search pattern you decide on covers all the ground and doesn't extend unreasonably far from the expected location of the patient.
Get as many details from EMS about the patient as possible including a physical description as well as symptoms.
A patient with a broken collarbone may be standing and otherwise appear well, and a patient who is suffering from heat stroke is probably on the ground and in the shade.

Before you and you team break up, pick a rally point in the event you are unable to find each other after finding the patient.
One option is meeting at the ambulance that EMS arrived in.
If the patient is evacuated, the attending medics can follow them back to the ambulance and meet the other medics.
Major landmarks are also a useful option.

Typically a search takes under 5 minutes, so medics who have not found the patient after thoroughly searching their area should move to other areas in an attempt to regroup with other medics.
This is useful because it allows teams to reform and move on to assist the action in other ways instead of waiting until the one patient has been fully assisted.

\index{search and rescue|)}

\section*{Be Where Help is Needed}

It may seem obvious to say ``be where help is needed,'' but often medics will make several mistakes and do what feels right over what may actually be useful.
They may have positioned themselves in the correct place, namely where there is violence between protesters and police or fascists, but surging violence may cause them to be unable to effectively render care.

Much of this is following instincts that must be manually overridden.
This can be done by constantly asking one's self ``What do I do if the police surge? What do I do if our side attacks? What do I do if we are attacked?''
Without an answer to this question, your first reaction may not be the most effective.

% TODO add a section that being alone is fine if people need help, don't just aimlessly regroup

\triplesubsection{Stay with the wounded}
When the protest breaks through a police line, your instinct may be to keep running with the group.
If police do not target medics, or if you are willing to risk arrest\index{arrest!medics@of medics} to render care, you should stay to help injured protesters.
Immediately stopping or quickly moving may be dangerous as people behind you may hit you.
Ease to the side and slow down, then double back to look for people to help.

\triplesubsection{Don't over-retreat}
When there is a wave of police violence, either through a charge or a volley of rubber bullets or tear gas, you instinct may be to run until you are safe.
Instead, consider only retreating just far enough to be out of direct fire.
Keep an eye out for protesters on the ground and pressed up against buildings for cover.
You may need to stay to help and evacuate injured protesters.

\triplesubsection{Too many cooks in the kitchen}
When someone becomes injured, it is common for all present medics to rush over.
This quickly leads to an over-saturation of medical care where everyone wants to feel like they're helping.
Sometimes this is useful as it means there will be people present who know to form a wall and help protect the medics who are attending to the patient.
However, medics should attempt to recruit non-medics to act as protection to free themselves up so they can look for and help other patients.
It feels good, brave, and bold to link arms and stand around while someone treats a patient, but in many cases the medics don't need protection and the gesture is purely performative.

\section*{Keeping a Low Profile}

Part of being a medic is avoiding becoming a focal point for either police, fascist, or friendly attention.
This may mean slightly altering your tactics from what is ideal for providing care to what is more useful for the goals of an action.

\subsection*{Don't Draw Attention}

As a medic, you may be well connected to the inner workings of the local anarchist or antifascist scene, and hence you may have inside knowledge about non-public actions or non-public tactics that will be used during actions.
If you typically mark up or are one of a minority of people with a large backpack at actions, the police will recognize you.
Thus, you being a part of an affinity group that is carrying out a non-public action may cause the group to lose the element of surprise.

If a group you know is planning an action or planning to use tactics during an action that require secrecy, you may have to give that group space to allow them to carry out their operation in secret.
At a large action, you may be able to position yourself somewhere where you can quickly move to support comrades, and for secret actions, you may be able to dress in street clothes and wait in a near by cafe until after the action has started.

It is important to remember that while a medic's job is to support action and provide care, that care should not get in the way of the actions themselves.
Medics need to remember that those partaking in an action have the agency to decide to engage in potentially risky activities, and insisting on providing care when it may be counterproductive to the action's goals can be paternal and infantilizing to the participants.

During actions, you may want to avoid pointing at individuals, at groups, or in the directions of group movement as this may be seen by the police and used against the participants.
You may know what is going on and have eyes on the entire scene, but as a medic you might not be in a position to direct the group, and you don't want to give away information.

\subsection*{Avoid Escalating}

You may have significantly more protective equipment than other protesters as this is ``allowed'' by the police because medics may not be considered protesters.
The importance of safety has been mentioned numerous times in this book, so you might suspect that medics are encouraged to always wear a helmet and have their respirators slung around their necks.
This is inadvisable as police may interpret it as either an escalation or a warning sign that the protest is about to turn violent.
Similarly, participants in the demonstration may see you fully prepared for violence and become uneasy as they suspect you know something they don't.

At most actions, if the police do not have helmets on, you probably don't need to wear one either.
Even if one bottle is thrown or one firecracker is set off, you may still want to leave your helmet off since other participants may look to medics for cues about how they should act.
For your respirator and goggles, you probably shouldn't don them until after tear gas is deployed the first time (though they should be easily accessible).

\section*{Maintaining Vigilance}

Staying safe during an action isn't simply a matter of medics wearing personal protective equipment and carefully positioning themselves.
It is also an active effort to be aware of their surroundings even when the action appears to be at rest.

\begin{figure}[htbp]
\centering
\caption{Diamond Formation\supercite{zerocoil}}
\label{fig:diamond_formation}
\includesvg[height=6cm, keepaspectratio]{diamond-formation}
\end{figure}

Medics should be aware of police and fascist presence.
If you are walking at tail end of a march, you should frequently look behind yourself to check that the sweeping police line is not closing in on you.
If it is, you may need to join the main body instead of walking behind it.
If you are on the sides of an action, groups of riot police may be walking around quickly and will knock people out of their way or snatch them for arrest\index{arrest}.
Beware of groups of riot police quickly moving up beside you.

If an action is standing still, it is useful for medics to stand back-to-back in a circle so each medic is looking in a different direction (\autoref{fig:diamond_formation}).
This is sometimes called a diamond formation.
When doing this, collectively the group has no blind spots.
This allows the medics to survey the entirety of their surroundings for patients as well as police or fascist movements.
However, this tactic may appear overly militaristic and may make people uneasy if used at actions where there is little to no chance of police violence or where there are no riot police present.

\section*{Summary}

It is exceedingly difficult to make statements about how any single medic or collective should act at an action.
Many of the factors one must consider are region specific.
While this chapter may not have been able to give you hard rules to always follow, some of the advice generalizes to many situations.
If all of this information were to be summed up, it could be be:

\begin{enumerate}
    \item Be safe, be alert.
    \item Move towards where you expect violence.
    \item Don't let a surging crowd pull you away from your duties.
\end{enumerate}

\index{tactics!action|)}
