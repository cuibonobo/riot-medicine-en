\chapter{Respiratory and Cardiac Emergencies}
\label{ch:respiratory_and_cardiac}

\epigraph{
I set aside my broken heart and I heal the only way I know how--by being useful.
I efficiently compartmentalize my pain\ldots
And I joyfully go about this work.
}{Willem van Spronsen\supercite{spronsen}}

\noindent
Along with wound management, recognition and treatment of respiratory and cardiac emergencies are the basis of most first aid courses.
These illnesses are often taught as primarily happening to individuals with genetic dispositions or in populations of high risk such as the elderly.
During actions, respiratory and cardiac emergencies may additionally happen as a complication from other injuries.
Traumatic chest injuries can lead to cardiac arrest, and exposure to riot control agents can lead to an asthma attack.

% TODO should include COPD as it is similar enough
\section*{Asthma}
\index{asthma|(}
\index{lungs!asthma@in asthma|(}

Asthma is a long term disease of the airways of the lungs that results in restricted airflow and difficulty breathing.
The development of asthma may be caused by exposure to pollution, cigarette smoke, molds, allergens, and certain chemicals.\footnotemark[1]
The main physiological changes are constriction of the smooth muscles of lower respiratory tract (bronchospasm), bronchial wall edema, and thick secretions into the airway.\supercite[p. 468-9]{tintinallis}
This narrowing of the airway is exists on a spectrum from acute airway obstruction to the remodeling of lung tissue leading to permanent loss of lung function\supercite[p. 469]{tintinallis} (though this happens over a longer time and is not something medics need to be concerned with).
Asthma exacerbation (\autoref{fig:asthma_airway}), commonly known as an asthma attack, is an acute reversible state that may return to normal spontaneously or with the help of medication.

\index{lungs!asthma@in asthma|)}

\footnotetext[1]{
``Western lifestyle'' is also cited as a factor that contributes to the development of asthma,\supercite{tintinallis, asthma-africa, asthma-western} so for all their whining about ``creating the modern world,'' dumbfuck white supremacists are literally ruining everyone's health because of their insistence on pursuing some myth.
Civilization as we know it is an untenable goal, and we should radically rethink how we structure our society.
}

\begin{figure}[htbp]
\caption{Airway Cross Section\supercite{baedr}}
\label{fig:asthma_airway}
\centering
\begin{subfigure}[b]{4cm}
    \centering
    \includesvg[width=\textwidth, height=6cm, keepaspectratio]{airway-cross-section-normal}
    \caption{Normal}
\end{subfigure}
\begin{subfigure}[b]{6cm}
    \centering
    \includesvg[width=\textwidth, height=6cm, keepaspectratio]{airway-cross-section-constricted}
    \caption{During asthma attack}
\end{subfigure}
\end{figure}

% TODO consider noting that not all respiratory distress is asthma (e.g., after pepper spray)

\subsection*{Signs and Symptoms}

In general, signs and symptoms of asthma exacerbation include shallow breathing (dyspnea), wheezing, and coughing.
Patients may be hunched over and unable to speak more than short sentences.

Early signs are a complaint of chest tightness and constriction as well as coughing.
As asthma exacerbation progresses, the patient may begin wheezing, have a prolonged expiration phase (inspiratory to expiratory ratio of at least 1 to 3), use accessory muscles to assist with breathing effort, and become cyanotic.
Wheezing may, however, only be audible with a stethoscope.
The patient's heart rate and respiratory rate may be elevated (\gls{tachycardia} and \gls{tachypnea}).
As asthma progresses, the use of accessory muscles due to diaphragmatic fatigue can cause paradoxical breathing\index{paradoxical breathing!asthma@in asthma}\footnotemark[2] where the abdomen\index{abdomen!asthma@in asthma} protrudes and the chest deflates during inspiration.
Paradoxical breathing and altered mental status such as lethargy and confusion are signs of impending respiratory arrest.\supercite[p. 469]{tintinallis}

% TODO graph of normal respiration lung volume vs. asthmatic respiration (prolonged expiration)

\footnotetext[2]{
Paradoxical breathing is an overloaded term that describes two different phenomena.
Paradoxical breathing due to diaphragm fatigue should not be confused with the paradoxical breathing due to flail chest.
For the latter, see \fullref{ch:chest_injuries}.
}

There are other conditions medics may encounter that mimic asthma, so you should attempt to perform a differential diagnosis to help guide treatment.
Conditions with similar signs and symptoms include upper airway obstruction, aspiration of foreign bodies or gastric acid, and multiple pulmonary emboli\index{pulmonary embolisms}.\supercite[p. 470]{tintinallis}

\subsection*{Treatment}

Treatment is optionally administer medication, to monitor the patient for signs of improvement or deterioration of their condition, and to consider evacuation to advanced medical care.

\triplesubsection{Move the patient away from irritants}
The patient should be moved away from sources of irritants such as riot control agent, pollen, or dust.

\triplesubsection{Decontaminate the patient}
If the patient is covered in allergens, do what you can to remove the allergens.
Specific steps for this can be found in \fullref{ch:rca_contamination}.

\triplesubsection{Sit the patient leaning forward}
In order to facilitate the use of the accessory muscles of respiration, sit the patient in a chair, have them lean forward, and position their hands on their knees.
This will give them stability for breathing.
Patients may instinctively want to lay down, but this should be avoided.

\triplesubsection{Remove constricting clothing}
If the patient is wearing clothing that could constrict their breathing or a backpack, remove and loosen what you can.
This will make it easier for them to breath, and will make it easier to perform basic life support should they go into respiratory or cardiac arrest.

\index{albuterol|see {salbutamol}}
\index{bronchodilators|seealso {salbutamol}}
\index{inhalers|see {salbutamol}}
\index{salbutamol|(}
\triplesubsection{Consider administering bronchodilators}
Bronchodilators are fast-acting medications that dilate the bronchi and bronchioles.
These are often inhaled via a \acrlong{MDI} (\acrshort{MDI}), commonly salbutamol (albuterol).
Inhalation of the medication is improved by the use of a spacer chamber (\autoref{fig:inhaler_usage}).

\begin{figure}[htbp]
\centering
\caption{Inhaler Usage\supercite{snailsnail}}
\label{fig:inhaler_usage}
\includesvg[height=6cm, keepaspectratio]{inhaler-usage}
\end{figure}

If the patient is asthmatic, consider helping them self administer their medication as they may be panicked or have an altered mental status.
If you carry salbutamol, consider offering it to them following the guidelines in \fullref{ch:medication}.

When assisting with administering inhaled bronchodilators, shake the \acrshort{MDI}, attach it to the spacer, and depress the cannister.
The patient should inhale deeply and hold their breath for 5 to 10 seconds.
Dosing will vary depending on the device and medication, though a reasonable rule to guide treatment is 2 puffs every 5 minutes, up to a total of 12 puffs.\supercite[p. 209]{nols}

\index{salbutamol|)}

\triplesubsection{Coach the patient on breathing}
Regulating the patient's breathing can help both calm them and relieve symptoms without medication.
Coach the patient on the pursed lip breathing technique using the following steps:

\begin{enumerate}
    \item The patient sits up right and relaxes their neck and shoulders.
    \item They inhale through their nose for 2 seconds. Their mouth should be closed.
    \item They purse (pucker) their lips as if blowing out a candle or whistling and exhale for 4 seconds.
    \item This is repeated until symptoms subside.
\end{enumerate}

The increased pressure during the expiratory phase helps open the airways of the lungs.
The deep inhalation and exhalation helps force out the ``stale'' air in their lungs.

\triplesubsection{Provide humid air}
Warm, humid air will help clear lung mucus.
If possible, move the patient somewhere with warmer and more humid air.
During remote actions, consider boiling water on a camp stove and having the patient breath the humidified air.

\index{status asthmaticus|seealso {asthma}}
\index{epinephrine!asthma@in asthma|(}
\triplesubsection{Consider administering epinephrine}
If the patient is having a severe asthma attack that does not respond to bronchodilators, they have status asthmaticus\index{status asthmaticus}.
Consider using an epinephrine autoinjector to administer a single dose of epinephrine.\supercite[p. 473]{tintinallis}.
See \fullref{ch:allergies_and_anaphylaxis} for instructions on how to do this.
\index{epinephrine!asthma@in asthma|)}

\triplesubsection{Consider evacuation}
If the patient has an altered mental status or is not improving after several minutes, evacuate the patient to advanced medical care.

\index{asthma|)}

\section*{Hyperventilation}
\index{hyperventilation|(}
\index{homeostasis!blood pH@of blood pH|(}

Homeostasis of blood pH is complex and involves a number of systems, though it can be rapidly affected by respiration.
When carbon dioxide is dissolved in water creates carbonic acid (H\textsubscript{2}CO\textsubscript{3}).
Carbon dioxide in the blood affects pH by making it more acidic.
During respiration, carbon dioxide is eliminated from the blood making it more alkaline.\supercite[p. 206]{nols}
Small variations in the amount of carbon dioxide dissolved in the blood are buffered by the bicarbonate buffer system\index{bicarbonate buffer system}.

\index{respiratory alkalosis|(}
Hyperventilation is increased respiratory rate or tidal volume that eliminates carbon dioxide from the blood faster than the body generates it.
Respiratory alkalosis is a decrease in the partial pressure of carbon dioxide in the blood (hypocapnia\index{hypocapnia}) due to an increased respiration.
Hypocapnia causes cerebral vasoconstriction leading to ischemia and cerebral hypoxia.\supercite{hypocapnia}
Prolonged hyperventilation also leads to respiratory alkalosis.
\index{respiratory alkalosis|)}

\index{homeostasis!blood pH@of blood pH|)}

Hyperventilation is often psychological in origin (stress, anxiety, panic), though it may be the result of brain damage (trauma, stroke), respiratory disorders (asthma, pneumonia), or cardiovascular disorders (pulmonary embolism, anemia).
Hyperventilation can happen as a result of injury regardless of severity, or it may happen after a traumatic situation in uninjured individuals.
Even before an action really kicks off, the presence of large crowds or police issuing warnings can lead to stress and hyperventilation, so medics should keep an eye out for panicked demonstrators.

\subsection*{Signs and Symptoms}

Patients may appear panicked, stressed, and anxious, though this is often the origin of the hyperventilation and not a symptom of it.
Typically the only visible sign is increased respiratory rate (\gls{tachypnea}) or both increased respiratory rate and depth (\gls{hyperpnea}).

Cerebral hypoxia due to hypocapnia\index{hypocapnia} can cause a feeling of light headedness and additional feelings of anxiety.
Patients may report a feeling of suffocation despite having a clear airway and normal blood oxygen saturation.
Cerebral hypoxia may eventually lead to syncope.

\index{respiratory alkalosis|(}
Due to respiratory alkalosis, patients may have a tingling feeling (paraesthesia) in their hands and around their mouth.
As respiratory alkalosis progresses, calcium ion imbalances leads to cramping and painful muscle spasms in the hands and feet (carpopedal spasms\index{carpopedal spasms}).
The patient's hands may curl inward and become unusable (tetany).
The patient may have stabbing pain in their chest.
\index{respiratory alkalosis|)}

\subsection*{Treatment}

Treatment is to calm the patient and help them regulate their breathing breathing.

\triplesubsection{Calm the patient}
Help treat the patient's stress or anxiety by calming them and offering reassurance.
Tell the patient that they need to slow their breathing and that you are there for them.
Explain to them that the symptoms they are feeling will go away once they start breathing normally.

\triplesubsection{Regulate their breathing}
Coach the patient on breathing to help them regulate their respiratory rate.
One option is to have them purse their lips and breath through a reduced opening.
Another is to get them to exhale for longer than they breath in.
This is often done through the 7/11 technique of breathing in slowly for 7 seconds followed by breathing out slowly for 11 seconds.
The patient should continue controlled breathing until symptoms subside.

\index{urban legends!hyperventilation@in hyperventilation|(}
\triplesubsection{Avoid use of bag rebreathing}
Urban legend suggests that patients should breath into a plastic or paper bag.
This has the effect of increasing the amount of alveolar carbon dioxide, though experimentally it does not lead to a change in the time until the disappearance of symptoms, thus leading to the idea that bag rebreathing is beneficial due to primarily psychological reasons.\supercite{hyperventilation-rebreathing}
While this treatment may be appropriate for patients who are hyperventilating due to anxiety, if it is erroneously applied to patients who are hyperventilating due to hypoxemia (low oxygen in the blood), it is ``capable of producing sudden deterioration and death.''\supercite{hyperventilation-death}
Medics should not use bag rebreathing to treat hyperventilation.\supercite[p. 206-7]{nols}
\index{urban legends!hyperventilation@in hyperventilation|)}

\index{hyperventilation|)}

\section*{Pulmonary Embolism}
\index{pulmonary embolisms|(}
\index{lungs!embolisms@in embolisms|(}

An embolism is the obstruction of a blood vessel by foreign matter such as a blood clot (thrombus) or gas bubbles.
A thrombus may form somewhere in the body, break off, and travel through blood vessels lodging somewhere else.
\index{deep vein thrombosis|(}
Deep vein thrombosis is the formation of a thrombus in the deep veins of the body, often in the legs or pelvis.
Thrombi that form in the legs can break off, travel through the right chambers of the heart, and lodge in the arteries of the lungs.
This is called a pulmonary embolism.
Most pulmonary embolisms are caused by deep vein thrombosis in the legs, pelvis, and arms.\supercite[p. 388]{tintinallis}
\index{deep vein thrombosis|)}

\begin{figure}[htbp]
\centering
\caption{Pulmonary Embolism\supercite{baedr}}
\includesvg[height=6cm, keepaspectratio]{pulmonary-embolism}
\end{figure}

The biggest risk factor for venous thromboembolism is prolonged limb immobility in two contiguous joints.\supercite[p. 390]{tintinallis}
Medics may see this during occupations, during lock-ons, or following long distance travel (6 continuous hours or more).
Other risk factors include being over 50 years old, smoking, obesity, and taking estrogen (such as in contraceptives)\index{estrogens!pulmonary embolisms@in pulmonary embolisms}.

\subsection*{Signs and Symptoms}

Patients may have sudden onset chest pain accompanied with shortness of breath (dyspnea).
Chest pain may be focal to the location of the embolism.
Pain may increase during inspiration.
The patient may sweat; have pale, cool, clammy skin; and go into shock\index{shock!pulmonary embolisms@in pulmonary embolisms}.
They may develop respiratory distress and become cyanotic, especially cyanosis of the lips and fingernail beds.
However, the presentation of a pulmonary embolism may only be rapid onset shortness of breath and coughing.

Pulmonary embolism may present similar to other illnesses such as cardiac problems and pneumothorax.
However, for all of these illnesses, treatment is evacuation.

\subsection*{Treatment}

As a medic, you cannot treat a pulmonary embolism.
Assist the patient with breathing, and monitor their ABCs.
Treat for shock.
Evacuate the patient to advanced medical care.

\index{lungs!embolisms@in embolisms|)}
\index{pulmonary embolisms|)}

% TODO ? \section*{Pneumonia}


\section*{Cardiac Chest Pain}
\index{cardiac chest pain|(}
\index{chest!cardiac chest pain@in cardiac chest pain|(}

Chest pain can be very cryptic as it can have many origins.
It may be related to comparatively innocuous injuries and illnesses such as a strained muscle, gastrointestinal distress, or rib fracture.
Other times it is related to life-threatening medical conditions like heart dysfunctions.

\begin{figure}[htbp]
\centering
\caption{Coronary Circulation\supercite{drnsx42}}
% public domain: https://commons.m.wikimedia.org/wiki/File:Heart_arteries_front-back.svg
\includesvg[height=4cm, keepaspectratio]{coronary-circulation}
\end{figure}

\index{heart attack|see {myocardial infarction}}

\index{acute coronary syndrome|(}
Acute coronary syndrome (\acrshort{ACS}) is a set of symptoms associated with reduced blood flow of the coronary arteries causing heart muscle to malfunction and die.
One cause of \acrshort{ACS} is myocardial infarction\index{myocardial infarction} (heart attack) where the arteries that supply the heart with blood become blocked.
Another cause is a sudden reduction blood flow (ischemia) of the heart muscle (unstable angina).
\index{acute coronary syndrome|)}

\index{angina pectoris|(}
Angina pectoris, often just angina, is chest pain due to ischemia of the heart muscle.
Stable angina is ischemic related chest pain that is brought on by exercise but abates shortly after stopping exercise.
Unstable angina is ischemic related chest pain that occurs when the patient is at rest, that is new and severe, or that has increasing severity or frequency.
\index{angina pectoris|)}

\subsection*{Signs and Symptoms}

So-called ``classic cardiac chest pain'' is often described as crushing pressure or a feeling of tightness in the chest that radiates toward the left arm and up toward the jaw.
Patients with \acrshort{ACS} do not always present with these symptoms, and some may have no pain at all.
This is especially true of women, non-white individuals, diabetic individuals, and elderly individuals.\footnotemark[3]
There are a number of signs and symptoms that indicate acute coronary syndrome, however a diagnosis, even by professionals, can be tricky and requires diagnostic equipment not available to medics.
This difficulty arising from the variation in presenting symptoms can be more clearly stated as such:\supercite[p. 326]{tintinallis}

\begin{displayquote}
There are no historical features with sufficient sensitivity and specificity to either diagnose or exclude acute coronary syndrome.
\end{displayquote}

\footnotetext[3]{Funny how ``typical'' seems to always mean middle aged, white men, innit?}

Patients with \acrshort{ACS} may have pain that is crushing and vice-like, a feeling of pressure, a feeling of chest tightness, or aching pain.
The pain may also be sharp and stabbing, and may be located in the chest, upper back, or shoulders.
The pain may radiate into either arm, both arms, or up into the neck and jaw.
Exertion may exacerbate this pain.
Cardiac chest pain in women may include upper abdomen pain and nausea.

Patients may have shortness of breath (dyspnea); have pale, cool skin; be sweating; or have nausea and be vomiting.
They may have feelings of anxiety, being grossly unwell, or impending doom (angor animi\index{angor animi}).

\subsection*{Treatment}

Patients with cardiac chest pain need to be examined by a physician, so a medic's job is to provide basic care and facilitate evacuation to advanced medical care.
Interview the patient about their medical history, and attempt to determine if the chest pain can be explained by non-cardiac factors.

\triplesubsection{Calm the patient}
Calm the patient and find somewhere for them to sit comfortably.
Helping the patient relax will reduce strain on their heart.

\index{aspirin!cardiac chest pain@in cardiac chest pain|(}
\triplesubsection{Consider administering aspirin}
If you carry medication, consider administering aspiring in line with the guidelines in \fullref{ch:medication}.
If the patient is not allergic to aspirin and has not had recent gastrointestinal bleeding, have them to chew 160 to \SI{325}{\mg} of aspirin.\supercite{acute-coronary-syndrome}
Chewing the tablet accelerates delivery of the medication.
\index{aspirin!cardiac chest pain@in cardiac chest pain|)}

\index{nitroglycerin!cardiac chest pain@in cardiac chest pain|(}
\triplesubsection{Remind patient to take heart medication}
If the patient has medication for cardiac chest pain such as nitroglycerin, remind them to take their medication.
Nitroglycerin comes in tablets that are placed under the patient's tongue, as a spray that is sprayed under the tongue, or as a topical medication that is applied to the skin.
\index{nitroglycerin!cardiac chest pain@in cardiac chest pain|)}

\triplesubsection{Treat for shock}
Patients with cardiac chest pain may go into cardiogenic shock\index{shock!cardiogenic!cardiac chest pain@in cardiac chest pain}.
Treat for shock as described in \fullref{ch:shock}.

\triplesubsection{Evacuate the patient}
Promptly evacuate the patient to advanced medical care.

\index{chest!cardiac chest pain@in cardiac chest pain|)}
\index{cardiac chest pain|)}

% TODO \section*{cardiac tamponade}

\section*{Summary}

Respiratory and cardiac emergencies can be life-threatening and may rapidly escalate from unwellness to deadly.
Asthma can be treated with breathing techniques and salbutamol, and in extreme cases it can be treated with epinephrine.
Hyperventilation should be treated by calming the patient and coaching them on breathing, but not with bag rebreathing.
Chest pain that cannot be explained by another injury or illness and respiratory distress that does not quickly improve are grounds for evacuation to advanced medical care.
